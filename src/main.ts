import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(
    session({
      name: 'mycustomcookie',
      saveUninitialized: true,
      secret: 'test',
      resave: false,
    }),
  );
  await app.listen(80);
}
bootstrap();
